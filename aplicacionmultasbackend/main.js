const express = require("express");
const app = express();
const mysql = require('mysql');
const cors = require('cors');
const axios= require('axios');
const bodyParser = require('body-parser');
const fs= require('fs');

const connection = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'appmultas'
});

app.use(express.json({limit: '50mb'}));

app.use(express.static('imagenes'));

app.use(cors());

connection.on("connection",(connection) => {
    
	
	
    console.log('Se conecto a la base de datos');


connection.on("error",(error) => {
    
	
	
    console.log(error);
});
});

app.get('/usuarios/', (request, response) => {
    
    connection.query('SELECT * FROM usuarios', (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});


app.get('/usuarios/:email', (request, response) => {
    const email = request.params.email;
    connection.query('SELECT * FROM usuarios WHERE email = ?', email, (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});

app.post('/usuarios', (request, response) => {

const usuario=request.body;
console.log(usuario);

    connection.query('insert into usuarios set ?', usuario, (error, result) => {
        if (error) console.log(error);
        response.status(201).send(result);
                             
		

    });
});


app.get('/documentos/', (request, response) => {
    
    connection.query('SELECT * FROM documentos', (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});


app.post('/carros', (request, response) => {

const carro=request.body;


    connection.query('insert into carros set ?', carro, (error, result) => {
        if (error) console.log(error);
        response.status(201).send(result);
    });
});

app.get('/carros/:email', (request, response) => {
    const email= request.params.email;
    connection.query('SELECT placa FROM carros WHERE email= ?', email, (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});

app.get('/carro/:placa', (request, response) => {
    const placa= request.params.placa;
    connection.query('SELECT * FROM carros WHERE placa= ?', placa, (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});

app.get('/placa/:documento', (request, response) => {

const documento=request.params.documento;
    
   let xmls=`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servicios/">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:consultarVehiculo>
         <!--Optional:-->
         <empresaGAD>22</empresaGAD>
         <!--Optional:-->
         <placa>${documento}</placa>
      </ser:consultarVehiculo>
   </soapenv:Body>
</soapenv:Envelope>`;

axios.post('http://10.201.70.50:5021/ConsumosVehiculosWS/VehiculoWS',
           xmls,
           {headers:
             {'Content-Type': 'text/xml'}
           }).then(res=>{
             console.log(res.data);
response.send(res.data);
           }).catch(err=>{console.log(err)});
});

app.get('/cargar', (request, response) => {
     let xmls=`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servicios/">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:consultarVehiculo>
         <!--Optional:-->
         <empresaGAD>22</empresaGAD>
         <!--Optional:-->
         <placa>${documento}</placa>
      </ser:consultarVehiculo>
   </soapenv:Body>
</soapenv:Envelope>`;

axios.post('http://10.201.70.50:5021/ConsumosVehiculosWS/VehiculoWS',
           xmls,
           {headers:
             {'Content-Type': 'text/xml'}
           }).then(res=>{
             console.log(res.data);
response.send(res.data);
           }).catch(err=>{console.log(err)});
});

app.get('/multas/:placa', (request, response) => {
    const placa = request.params.placa;
    connection.query('SELECT * FROM multas WHERE placa = ?', placa, (error, result) => {
        if (error) throw error;
        response.send(result);
    });
});

app.post('/multas', (request, response) => {

const multa=request.body;
console.log(multa);

    connection.query('insert into multas set ?', multa, (error, result) => {
        if (error) console.log(error);
        response.status(201).send(result);
                             
		

    });
});

app.post('/documentos', (request, response) => {

const documento=request.body;



documentos(documento);



    
});

async function documentos(documento) {




var base64Data = await documento.imagen.replace(/^data:image\/png;base64,/, "");


await fs.writeFile(`imagenes/${documento.email}-${documento.placa}-${documento.tipo}.jpeg`, base64Data , 'base64', function(err) {



});





   
}






app.listen(5200,'192.168.50.16', () => {
    console.log('El servidor esta en el puerto ');
});