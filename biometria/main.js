const express = require('express');
const app = express();
const mysql = require('mysql');
const cors = require('cors');
const axios = require('axios');
bodyParser = require('body-parser');
const FormData = require('form-data');
const fs = require('fs');
const base64 = require('byte-base64');

app.use(express.json({ limit: '50mb' }));

app.use(cors());

app.post('/usuarios', (request, response) => {
	const imagen = request.body;

	grabar2(imagen, response);
});

async function grabar2(imagen, response) {
	var base64Data = await imagen.imagen.replace(
		/^data:image\/png;base64,/,
		''
	);

	await fs.writeFile(
		`/imagenes/${imagen.cedula}_imagen.jpeg`,
		base64Data,
		'base64',
		function (err) {}
	);

	var base64Data2 = await imagen.imagen2.replace(
		/^data:image\/png;base64,/,
		''
	);

	await fs.writeFile(
		`/imagenes/${imagen.cedula}_imagen2.jpeg`,
		base64Data2,
		'base64',
		function (err) {}
	);

	var base64Data1 = await imagen.imagen1.replace(
		/^data:image\/png;base64,/,
		''
	);

	await fs.writeFile(
		`/imagenes/${imagen.cedula}_imagen1.jpeg`,
		base64Data1,
		'base64',
		function (err) {
			let data = new FormData();
			data.append(
				'imagenBase64',
				fs.createReadStream(
					`/root/autoapp/biometria/imagenes/${imagen.cedula}_imagen1.jpeg`
				)
			);
			data.append(
				'imagenDocumento',
				fs.createReadStream(
					`/root/autoapp/biometria/imagenes/${imagen.cedula}_imagen1.jpeg`
				)
			);
			data.append(
				'imagenVideo',
				fs.createReadStream(
					`/root/autoapp/biometria/imagenes/${imagen.cedula}_imagen2.jpeg`
				)
			);
			data.append(
				'imagenSelfie',
				fs.createReadStream(
					`/root/autoapp/biometria/imagenes/${imagen.cedula}_imagen.jpeg`
				)
			);
			data.append(
				'pruebaVida',
				`{"idLogPruebaVida":0,"idUsuario":0,"correo":"${imagen.email}","pathPruebaVida":null,"esVideo":false,"esImage":true,"ipRegistro":"::1","FechaRegistro":null,"contenType":"imagen/png","extension":".png","cedulaOcr":"${imagen.cedula}","video":null,"imagen":null}`,
				{ contentType: 'application/json' }
			);

			let config = {
				method: 'post',
				maxBodyLength: Infinity,
				url: 'https://sodignature.com/ApiSodig/api/Informacion/VerificarPruebaVida',
				headers: {
					Authorization: `Bearer ${imagen.token}`,
					...data.getHeaders(),
				},
				data: data,
			};

			axios
				.request(config)
				.then((res) => {
					console.log(res.data);
					console.log(res.data.obj.verificacion);

					response.send(res.data);
				})
				.catch((error) => {
					console.log(error);

					response.send(error);
				});
		}
	);
}

app.listen(5201, '192.168.50.16', () => {
	console.log('El servidor esta en el puerto ');
});
